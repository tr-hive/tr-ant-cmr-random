 /// <reference path="../typings/tsd.d.ts" />
import rabbit = require("da-rabbitmq-rx");
import logs = require("da-logs");
import handler = require("./handler")
 
function getEnvVar(name: string) : string {
	return process.env[name] || 
	process.env["npm_config_" + name] || 
	process.env["npm_package_config_" + name];	
}

const RABBIT_URI = getEnvVar("RABBIT_URI");
const RABBIT_QUEUE = getEnvVar("RABBIT_QUEUE");

var logger = new logs.LoggerCompose({pack : <any>require("../package.json"), tags : []},  {
    loggly: {token: getEnvVar("LOG_LOGGLY_KEY"), subdomain: getEnvVar("LOG_LOGGLY_SUBDOMAIN")},
    mongo: {connection: getEnvVar("LOG_MONGO_URI"), collection: getEnvVar("LOG_MONGO_COLLECTION")},
    console: true
 });


var pubOpts = {uri: getEnvVar("RABBIT_URI"), socketType: rabbit.SocketType.PUB, queue: getEnvVar("RABBIT_QUEUE_COMMANDS")};
var pub = new rabbit.RabbitPub(pubOpts); 
pub.connect();
pub.connectStream.subscribe(() =>
 logger.write({resource: "rabbit", oper: "connected", status : "success", opts: pubOpts}) 
, (err) => {
	logger.write({resource: "rabbit", status : "error", err: err, opts: pubOpts});
	process.exit(1);
});

 
//BUY 
const BUY_INTERVAL = parseInt(getEnvVar("BUY_INTERVAL"));
const BUY_TICKETS = getEnvVar("BUY_TICKETS").split(",");
handler.emitRandom({
	logger: logger,
	pub: pub,
	tickets: BUY_TICKETS,
	interval: BUY_INTERVAL
});  

//SELL
const SELL_INTERVAL = parseInt(getEnvVar("CMR_RANDOM_SELL_INTERVAL"));
var subOpts = {uri: RABBIT_URI, socketType: rabbit.SocketType.SUB, queue: getEnvVar("RABBIT_QUEUE_NOTIFS")};
var sub = new rabbit.RabbitSub(subOpts); 
sub.connect();
sub.stream.take(1).subscribe(() =>
 logger.write({resource: "rabbit", oper: "connected", status : "success", opts: subOpts}) 
, (err) => {
	logger.write({resource: "rabbit", oper: "connected", status : "error", err: err, opts: subOpts});
	process.exit(1);
});

handler.sellPortfolio({
	logger: logger,
	notifStream: sub.stream.skip(1), 
	notifIssuer: getEnvVar("SELL_NOTIF_ISSUER_NAME"),
	pub: pub,
	sellInterval: SELL_INTERVAL
})
.subscribe(() => {},
(err) => {
	logger.write({resource: "handler", oper: "runtime", status : "error", err: err});
	process.exit(1);	
}, () => {
	logger.write({resource: "handler", oper: "runtime", status : "success"});
	process.exit(0);		
})  

