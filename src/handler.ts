/// <reference path="../typings/tsd.d.ts" />
import logs = require("da-logs");
import Rx = require("rx");
import rabbit = require("da-rabbitmq-rx");
var uuid = require("node-uuid");

const PKG_NAME = require("../package.json").name;
var SECL_IST = require("../data/SEC_LIST.json");

function tulpe<T1, T2>(val1: T1, val2: T2) : trAnt.ITulpe<T1, T2> {return {val1 : val1, val2: val2}}

export interface IEmitRandomOpts {
	
	/**
	 * Intrval to publish messages in ms
	 */
	interval : number
	/**
	 * Tickets from where select random ticket to publish
	 */
	tickets: string[]	
	/**
	 * Logger
	 */
	logger: logs.ILogger
	
	/**start
	 * Publisher
	 */				
	pub: rabbit.RabbitPub
}

function getRandom(min: number, max: number): number {
  return Math.round(Math.random() * (max - min) + min);
}

export function emitRandom(opts: IEmitRandomOpts) : Rx.IDisposable {

	return Rx.Observable.interval(opts.interval).subscribe(() => { 
		var idx = getRandom(0, opts.tickets.length - 1);
		var instr = opts.tickets[idx];
		var instrLot = SECL_IST[instr];  
	
		var command : trAnt.ICmd = 		
		{
			key : uuid.v4() + "_" + PKG_NAME,
			issuer : PKG_NAME,
			date: (new Date()).toISOString(),
			reason: "random buy command",
			portfolio: PKG_NAME,
				trade : {
				ticket: instr,
				oper: "buy",
				quantity: instrLot 
				}			
		}	
		
		opts.pub.write(command);
		
		opts.logger.write({oper: "cmd", status : "success", data : { cmd : command}});	
	});	 		
}

export interface ISellPortfolioOpts {
	notifStream: Rx.Observable<trAnt.INotif<trAnt.INotifPortfolioChanged>>
	pub: rabbit.RabbitPub	
	logger: logs.ILogger
	/**
	 * Handle only notifications from this issuer
	 */
	notifIssuer: string	
	sellInterval: number
}


export function sellPortfolio(opts: ISellPortfolioOpts) : Rx.IObservable<trAnt.ICmd> {
 	
	return opts.notifStream
	.filter(val => val.type == "INotifPortfolioChanged" && val.issuer == opts.notifIssuer)
	.do(val => 		
		opts.logger.write({oper: "cmd", status : "start", data : {notif : val}})
	)/*
	.flatMap(val => 
		Rx.Observable.fromArray(
			val.data.positions.map(x => tulpe(x, val))))
	*/			
	.filter(f => f.data.cmd.trade.oper == "buy")
	.map(notif => { 
		var command : trAnt.ICmd = 		
		{
			key : "[" + notif.key + "]_" + PKG_NAME,
			issuer : PKG_NAME,
			date: (new Date()).toISOString(),
			reason: "sell any position in portfolio",
			notif: notif,
			portfolio: notif.data.portfolio,
				trade : {
				ticket: notif.data.cmd.trade.ticket,
				oper: "sell",
				quantity: notif.data.cmd.trade.quantity 
			}			
		}		
		return tulpe(command, notif);			
	})
	.delay(opts.sellInterval)
	.do(val => { 
		opts.pub.write(val.val1);
		opts.logger.write({oper: "cmd", status : "success", data : {cmd : val.val1, notif : val.val2}});
	})	
	.map(val => val.val1);	
}