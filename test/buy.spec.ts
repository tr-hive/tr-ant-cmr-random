/// <reference path="../typings/tsd.d.ts"/>
/// <reference path="../dist/handler.d.ts"/>
import chai = require('chai'); 
import handler = require('../dist/handler');
import rabbit = require('da-rabbitmq-rx');
var expect = chai.expect;

function getEnvVar(name: string) : string {
	return process.env[name] || 
	process.env["npm_config_" + name] || 
	process.env["npm_package_config_" + name];	
}

describe("buy random tickets from list",  () => {

	it("emit random buy commands / subscribe and listen / validate",  (done) => {
		
		//publisher
		var pubOpts = {uri: getEnvVar("RABBIT_URI"), socketType: rabbit.SocketType.PUB, queue: getEnvVar("RABBIT_QUEUE_COMMANDS")};
		var pub = new rabbit.RabbitPub(pubOpts); 
		pub.connect();

		//subscribe here and validate all publishing
		var subOpts = {uri: getEnvVar("RABBIT_URI"), socketType: rabbit.SocketType.SUB, queue: getEnvVar("RABBIT_QUEUE_COMMANDS")};
		var sub = new rabbit.RabbitSub(subOpts); 
		sub.connect();
		var disposable; 
		sub.stream.take(1).subscribe(() => { 
			disposable = handler.emitRandom({
				logger: {write : (msg) => console.log(msg) },
				pub: pub,
				tickets: ["SBER"],
				interval: 1000
			})});
			
					
		//validate
		var count = 0; 			
		var disposable2 = sub.stream.skip(1).subscribe((val) => {
			expect(val).has.property("key");
			expect(val).has.property("issuer", "tr-ant-cmr-random");
			expect(val).has.property("portfolio", "tr-ant-cmr-random");
			expect(val).has.property("date");
			expect(val).has.property("trade");
			expect(val.trade).has.property("ticket", "SBER");
			expect(val.trade).has.property("oper", "buy");
			expect(val.trade).has.property("quantity", 10);
			count++;
		})
		
		setTimeout(() => {
			disposable.dispose();
			disposable2.dispose();
			expect(count).eq(3);
			done();
		}, 4000);		  																															
	})
	
	it("emit random buy commands / subscribe and listen / validate 2",  (done) => {
		
		//publisher
		var pubOpts = {uri: getEnvVar("RABBIT_URI"), socketType: rabbit.SocketType.PUB, queue: getEnvVar("RABBIT_QUEUE_COMMANDS")};
		var pub = new rabbit.RabbitPub(pubOpts); 
		pub.connect();

		//subscribe here and validate all publishing
		var subOpts = {uri: getEnvVar("RABBIT_URI"), socketType: rabbit.SocketType.SUB, queue: getEnvVar("RABBIT_QUEUE_COMMANDS")};
		var sub = new rabbit.RabbitSub(subOpts); 
		sub.connect();

		var disposabe;
		sub.stream.take(1).subscribe(() => { 
			disposabe = handler.emitRandom({
				logger: {write : (msg) => console.log(msg) },
				pub: pub,
				tickets: ["LKOH"],
				interval: 100
			})});
			
					
		//validate
		var count = 0; 			
		var disposable2 = sub.stream.skip(1).subscribe((val) => {
						
			expect(val).has.property("key");
			expect(val).has.property("issuer", "tr-ant-cmr-random");
			expect(val).has.property("portfolio", "tr-ant-cmr-random");
			expect(val).has.property("date");
			expect(val).has.property("trade");
			expect(val.trade).has.property("ticket", "LKOH");
			expect(val.trade).has.property("oper", "buy");
			expect(val.trade).has.property("quantity", 1);
			count++;
		})
		
		setTimeout(() => {
			disposabe.dispose();
			disposable2.dispose();
			expect(count).eq(37);
			done();
		}, 4000);		  																															
	})
	
});		