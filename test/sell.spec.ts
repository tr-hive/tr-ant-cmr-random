/// <reference path="../typings/tsd.d.ts"/>
/// <reference path="../dist/handler.d.ts"/>
import chai = require('chai'); 
import handler = require('../dist/handler');
import rabbit = require('da-rabbitmq-rx');
import Rx = require("rx");
var expect = chai.expect;

function getEnvVar(name: string) : string {
	return process.env[name] || 
	process.env["npm_config_" + name] || 
	process.env["npm_package_config_" + name];	
}

describe("sell all portfolio positions on on-portfolio-changed notification",  () => {

	it.skip("invalid / recieve portfolio-changed notif -> send sell all positions commands",  (done) => {

		//recieved portfolio changed message
		var portf : trAnt.INotif<trAnt.INotifPortfolioChanged> = {
			key : "1",
			issuer: "test",
			date: (new Date()).toISOString(),
			type: "INotifPortfolioChanged",
			data : {
				cmd: null,
				portfolio: "test-portf",
				value: 1000,
				positions: [
					{ticket: "SBER", quantity: 100, value : 20},
					{ticket: "VTBR", quantity: 200, value : 30}
				]
			}			
		}
		
				
		
		//publisher
		var pubOpts = {uri: getEnvVar("RABBIT_URI"), socketType: rabbit.SocketType.PUB, queue: getEnvVar("RABBIT_QUEUE_COMMANDS")};
		var pub = new rabbit.RabbitPub(pubOpts); 
		pub.connect();

		//subscribe here and validate all publishing
		var subOpts = {uri: getEnvVar("RABBIT_URI"), socketType: rabbit.SocketType.SUB, queue: getEnvVar("RABBIT_QUEUE_COMMANDS")};
		var sub = new rabbit.RabbitSub(subOpts); 
		sub.connect();

		var portfStream = Rx.Observable.create<trAnt.INotif<trAnt.INotifPortfolioChanged>>(observer => {
			observer.onNext(portf);
		});	
		
		var opts : handler.ISellPortfolioOpts = {
			notifStream: portfStream,
			notifIssuer: "test",
			pub: pub,
			logger: {write : (msg) => console.log(msg)},
			sellInterval: 0	
		};

		
		sub.stream.take(1).map(_ => 							
			handler.sellPortfolio(opts)			
		)
		.flatMap((val: any) => val)
		.subscribe(val => {})
		 
		sub.stream.skip(1).take(1).subscribe((val: trAnt.ICmd) => {
			expect(val).has.property("key");
			expect(val).has.property("issuer", "tr-ant-cmr-random");
			expect(val).has.property("date");
			expect(val).has.property("portfolio", "test-portf");
			expect(val).has.property("trade");
			expect(val).has.property("notif");
			expect(val).has.property("reason");
			expect(val.notif).eqls(portf);
			expect(val.trade).eqls({ticket: "SBER", oper: "sell", quantity: 100});					 
		});

		sub.stream.skip(2).take(1).subscribe((val: trAnt.ICmd) => {
			expect(val).has.property("key");
			expect(val).has.property("issuer", "tr-ant-cmr-random");
			expect(val).has.property("date");
			expect(val).has.property("portfolio", "test-portf");
			expect(val).has.property("trade");
			expect(val).has.property("notif");
			expect(val).has.property("reason");
			expect(val.notif).eqls(portf);
			expect(val.trade).eqls({ticket: "VTBR", oper: "sell", quantity: 200});
			
			done();					 
		});
					
	})
	
});		