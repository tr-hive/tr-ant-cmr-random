# Trader micro service

### Commander

Run : random from list, min lot size at 10 sec interval
Sell : on "portfolio-changed" notification, sell all items in portfolio

## Project structure

+ /src - sources, type script  
  + index.ts - entry point of application
    
+ Dockerfile - docker image builder
+ .envs - define you config variables here (ignored in git)
+ typings - typescript definition files    
  
## Prerequisites

+ nodejs
+ npm
+ typescript


## Install

```
git clone https://bitbucket.org/tr-hive/tr-ant-cmr-random
npm install
```  
+ To rebuild `tsc` or `npm run-script build`
+ To watch and rebuild `tsc -w`

Rebuild also executed before each `npm start` 

## Start

+ Define enviroment variables, in order of priority `env`, `.npmrc`, `package.json`
+ Test start `npm start` 

## Docker 

+ Build container `docker build -t baio/tr-ant-cmr-random .`
+ Start container `docker run baio/tr-ant-cmr-random`

Also build `npm run-script docker` or `npm run-script docker-clean`  