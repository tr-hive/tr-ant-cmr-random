/// <reference path="../typings/tsd.d.ts" />
import logs = require("da-logs");
import Rx = require("rx");
import rabbit = require("da-rabbitmq-rx");
export interface IEmitRandomOpts {
    interval: number;
    tickets: string[];
    logger: logs.ILogger;
    pub: rabbit.RabbitPub;
}
export declare function emitRandom(opts: IEmitRandomOpts): Rx.IDisposable;
export interface ISellPortfolioOpts {
    notifStream: Rx.Observable<trAnt.INotif<trAnt.INotifPortfolioChanged>>;
    pub: rabbit.RabbitPub;
    logger: logs.ILogger;
    notifIssuer: string;
    sellInterval: number;
}
export declare function sellPortfolio(opts: ISellPortfolioOpts): Rx.IObservable<trAnt.ICmd>;
