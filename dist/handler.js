var Rx = require("rx");
var uuid = require("node-uuid");
var PKG_NAME = require("../package.json").name;
var SECL_IST = require("../data/SEC_LIST.json");
function tulpe(val1, val2) { return { val1: val1, val2: val2 }; }
function getRandom(min, max) {
    return Math.round(Math.random() * (max - min) + min);
}
function emitRandom(opts) {
    return Rx.Observable.interval(opts.interval).subscribe(function () {
        var idx = getRandom(0, opts.tickets.length - 1);
        var instr = opts.tickets[idx];
        var instrLot = SECL_IST[instr];
        var command = {
            key: uuid.v4() + "_" + PKG_NAME,
            issuer: PKG_NAME,
            date: (new Date()).toISOString(),
            reason: "random buy command",
            portfolio: PKG_NAME,
            trade: {
                ticket: instr,
                oper: "buy",
                quantity: instrLot
            }
        };
        opts.pub.write(command);
        opts.logger.write({ oper: "cmd", status: "success", data: { cmd: command } });
    });
}
exports.emitRandom = emitRandom;
function sellPortfolio(opts) {
    return opts.notifStream
        .filter(function (val) { return val.type == "INotifPortfolioChanged" && val.issuer == opts.notifIssuer; })
        .do(function (val) {
        return opts.logger.write({ oper: "cmd", status: "start", data: { notif: val } });
    })
        .filter(function (f) { return f.data.cmd.trade.oper == "buy"; })
        .map(function (notif) {
        var command = {
            key: "[" + notif.key + "]_" + PKG_NAME,
            issuer: PKG_NAME,
            date: (new Date()).toISOString(),
            reason: "sell any position in portfolio",
            notif: notif,
            portfolio: notif.data.portfolio,
            trade: {
                ticket: notif.data.cmd.trade.ticket,
                oper: "sell",
                quantity: notif.data.cmd.trade.quantity
            }
        };
        return tulpe(command, notif);
    })
        .delay(opts.sellInterval)
        .do(function (val) {
        opts.pub.write(val.val1);
        opts.logger.write({ oper: "cmd", status: "success", data: { cmd: val.val1, notif: val.val2 } });
    })
        .map(function (val) { return val.val1; });
}
exports.sellPortfolio = sellPortfolio;
