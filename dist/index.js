/// <reference path="../typings/tsd.d.ts" />
var rabbit = require("da-rabbitmq-rx");
var logs = require("da-logs");
var handler = require("./handler");
function getEnvVar(name) {
    return process.env[name] ||
        process.env["npm_config_" + name] ||
        process.env["npm_package_config_" + name];
}
var RABBIT_URI = getEnvVar("RABBIT_URI");
var RABBIT_QUEUE = getEnvVar("RABBIT_QUEUE");
var logger = new logs.LoggerCompose({ pack: require("../package.json"), tags: [] }, {
    loggly: { token: getEnvVar("LOG_LOGGLY_KEY"), subdomain: getEnvVar("LOG_LOGGLY_SUBDOMAIN") },
    mongo: { connection: getEnvVar("LOG_MONGO_URI"), collection: getEnvVar("LOG_MONGO_COLLECTION") },
    console: true
});
var pubOpts = { uri: getEnvVar("RABBIT_URI"), socketType: rabbit.SocketType.PUB, queue: getEnvVar("RABBIT_QUEUE_COMMANDS") };
var pub = new rabbit.RabbitPub(pubOpts);
pub.connect();
pub.connectStream.subscribe(function () {
    return logger.write({ resource: "rabbit", oper: "connected", status: "success", opts: pubOpts });
}, function (err) {
    logger.write({ resource: "rabbit", status: "error", err: err, opts: pubOpts });
    process.exit(1);
});
var BUY_INTERVAL = parseInt(getEnvVar("BUY_INTERVAL"));
var BUY_TICKETS = getEnvVar("BUY_TICKETS").split(",");
handler.emitRandom({
    logger: logger,
    pub: pub,
    tickets: BUY_TICKETS,
    interval: BUY_INTERVAL
});
var SELL_INTERVAL = parseInt(getEnvVar("CMR_RANDOM_SELL_INTERVAL"));
var subOpts = { uri: RABBIT_URI, socketType: rabbit.SocketType.SUB, queue: getEnvVar("RABBIT_QUEUE_NOTIFS") };
var sub = new rabbit.RabbitSub(subOpts);
sub.connect();
sub.stream.take(1).subscribe(function () {
    return logger.write({ resource: "rabbit", oper: "connected", status: "success", opts: subOpts });
}, function (err) {
    logger.write({ resource: "rabbit", oper: "connected", status: "error", err: err, opts: subOpts });
    process.exit(1);
});
handler.sellPortfolio({
    logger: logger,
    notifStream: sub.stream.skip(1),
    notifIssuer: getEnvVar("SELL_NOTIF_ISSUER_NAME"),
    pub: pub,
    sellInterval: SELL_INTERVAL
})
    .subscribe(function () { }, function (err) {
    logger.write({ resource: "handler", oper: "runtime", status: "error", err: err });
    process.exit(1);
}, function () {
    logger.write({ resource: "handler", oper: "runtime", status: "success" });
    process.exit(0);
});
